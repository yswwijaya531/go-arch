package main

import (
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	articleDelivery "go-arch/app/delivery"
)

func main() {

	// Echo instance
	e := echo.New()

	// Middleware
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())

	// Route => handler
	articleDelivery.NewArticleHandler(e)

	// Start server
	e.Logger.Fatal(e.Start(":9001"))

}

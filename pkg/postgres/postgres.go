package postgres

import (
	"database/sql"
	"fmt"
	cn "go-arch/config"
	"sync"
)

var (
	db     *sql.DB
	oncePg sync.Once
)

func PGInit() *sql.DB {
	var err error

	conf := cn.UserConfig()

	psqlconn := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable", conf.Host, conf.Port, conf.User, conf.Password, conf.Password)

	oncePg.Do(func() {
		db, err = sql.Open("postgres", psqlconn)
		if err != nil {
			panic(err)
		}
	})

	return db
}
